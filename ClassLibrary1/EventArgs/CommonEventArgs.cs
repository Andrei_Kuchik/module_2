﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class CommonEventArgs: System.EventArgs
    {
        public bool FlagToRun { get; set; }
    }
}
