﻿using ClassLibrary1.EventArgs;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class FileSystemVisitor
    {
        private string pathFolder;

        private bool flagToRun = true;

        private Func<FileInfo, bool> filterForFile;

        private Func<DirectoryInfo, bool> filterForDirectory;

        public event EventHandler StartExecuting;

        public event EventHandler FinishExecuting;

        public event EventHandler<FileEventArgs> FileFinded;

        public event EventHandler<DirectoryEventArgs> DirectoryFinded;

        public event EventHandler<FileEventArgs> FilteredFileFinded;

        public event EventHandler<DirectoryEventArgs> FilteredDirectoryFinded;

        public FileSystemVisitor(string path)
        {
            if (String.IsNullOrWhiteSpace(path))
                throw new ArgumentException();

            pathFolder = path;
        }

        public FileSystemVisitor(string path, Func<FileInfo, bool> filterForFile, Func<DirectoryInfo, bool> filterForDirectory) : this (path)
        {
            if (filterForFile == null && filterForDirectory == null)
                throw new ArgumentNullException();

            this.filterForFile = filterForFile;
            this.filterForDirectory = filterForDirectory;
        }

        public IEnumerable<FileSystemInfo> GetAllFoldersAndFiles()
        {
            OnStart();
            Queue<string> folders = new Queue<string>();
            string currentPath = pathFolder;

            List<DirectoryInfo> directories = new List<DirectoryInfo>();
            List<FileInfo> files = new List<FileInfo>();

            while (flagToRun)
            {
                try
                {
                    var dir = new DirectoryInfo(currentPath);
                    directories = new List<DirectoryInfo>(dir.GetDirectories());
                    files = new List<FileInfo>(dir.GetFiles());
                }
                catch(UnauthorizedAccessException ex)
                {
                    Console.WriteLine(ex.Message);
                }
                catch(DirectoryNotFoundException ex)
                {
                    throw new ArgumentException("This path not found");
                }

                foreach (var file in files)
                {
                    OnFileFinded(file);
                    bool accessFilter = true;
                    if (filterForFile != null)
                    {
                        accessFilter = filterForFile(file);
                    }
                    if (accessFilter)
                    {
                        OnFilteredFileFinded(file);
                    }

                    yield return file;

                    if (!flagToRun)
                    {
                        yield break;
                    }
                }

                foreach (var directory in directories)
                {
                    OnDirectoryFinded(directory);
                    bool accessFilter = true;

                    if (filterForDirectory != null)
                    {
                        accessFilter = filterForDirectory(directory);
                    }
                    if (accessFilter)
                    {
                        OnFilteredDirectoryFinded(directory);
                    }

                    yield return directory;

                    if (!flagToRun)
                    {
                        yield break;
                    }

                    folders.Enqueue(directory.FullName);
                }

                if (!folders.Any())
                {
                    yield break;
                }

                currentPath = folders.Dequeue();
            }

            OnFinish();
        }

        protected virtual void OnStart()
        {
            StartExecuting?.Invoke(this, System.EventArgs.Empty);
        }

        protected virtual void OnFinish()
        {
            FinishExecuting?.Invoke(this, System.EventArgs.Empty);
        }

        protected virtual void OnFileFinded(FileInfo file)
        {
            ExecuteEventForFile(FileFinded, file);
        }

        protected virtual void OnDirectoryFinded(DirectoryInfo directory)
        {
            ExecuteEventForDirectory(DirectoryFinded, directory);
        }

        protected virtual void OnFilteredFileFinded(FileInfo file)
        {
            ExecuteEventForFile(FilteredFileFinded, file);
        }

        protected virtual void OnFilteredDirectoryFinded(DirectoryInfo directory)
        {
            ExecuteEventForDirectory(FilteredDirectoryFinded, directory);
        }

        private void ExecuteEventForFile(EventHandler<FileEventArgs> eventForFile, FileInfo file)
        {
            if (eventForFile != null)
            {
                FileEventArgs args = new FileEventArgs
                {
                    FlagToRun = true,
                    File = file
                };
                eventForFile?.Invoke(this, args);
                flagToRun = args.FlagToRun;
            }
        }

        private void ExecuteEventForDirectory(EventHandler<DirectoryEventArgs> eventForDirectory, DirectoryInfo directory)
        {
            if (eventForDirectory != null)
            {
                DirectoryEventArgs args = new DirectoryEventArgs
                {
                    FlagToRun = true,
                    Directory = directory
                };
                eventForDirectory?.Invoke(this, args);
                flagToRun = args.FlagToRun;
            }
        }
    }
}
